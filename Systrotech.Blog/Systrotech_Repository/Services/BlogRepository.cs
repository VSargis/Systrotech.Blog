﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Systrotech_Repository.Entities;

namespace Systrotech_Repository.Services
{
    public class BlogRepository : IBlogRepository
    {
        public BlogRepository()
        {
            _context = new BlogEntities();
        }
        protected BlogEntities _context;


        public List<TModel> SelectAll<TModel>() where TModel : class
        {
            return _context.Set<TModel>().ToList();
        }
        public TModel GetById<TModel>(int id) where TModel : class
        {
            return _context.Set<TModel>().Find(id);
        }

        public bool DeleteById<TModel>(int id) where TModel : class
        {
            try
            {
                TModel ob = GetById<TModel>(id);
                _context.Set<TModel>().Remove(ob);
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
           
        }

        //public bool Insert<TModel>() where TModel : class
        //{
        //    _context.Set<TModel>().Add();
        //}

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // NVI
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_context != null)
                {
                    _context.Dispose();
                    _context = null;
                }
            }
        }

        
    }
}
