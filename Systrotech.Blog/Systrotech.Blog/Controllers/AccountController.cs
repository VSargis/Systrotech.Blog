﻿using CaptchaMvc.HtmlHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Systrotech.Blog.Helpers;
using Systrotech.Blog.Mappers;
using Systrotech.Blog.View_Models;
using Systrotech_Repository.Entities;

namespace Systrotech.Blog.Controllers
{
    public class AccountController : Controller
    {
        // harcnel estexa chisht new anel te henc  funkciayi mej 
        private BlogEntities context = new BlogEntities();


        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterViewModel model)
        {
            // Code for validating the CAPTCHA  
            if (this.IsCaptchaValid("Captcha is not valid"))
            {
                if (ModelState.IsValid)
                {
                    User user = Mapper.ToRegisterViewModel(model);
                    context.Users.Add(user);
                    context.SaveChanges();

                    return RedirectToAction("Login", "Account");
                }
                return View(model);
            }
            else
            {
                ViewBag.ErrMessage = "Error: captcha is not valid.";
                return RedirectToAction("Register", "Account");
            }

        }


        public ActionResult Login()
        {
            LoginViewModel account = CheckCookie();
            if (account == null)
                return View();
            else
            {
                if (Helper.Checklogin(account.UserName, account.Password))
                {
                    Session["username"] = account.UserName;
                    return RedirectToAction("Page", "User");

                }
                else
                {
                    ViewBag.Error = "Account's Invalid";
                    return View();
                }
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model)
        {


            if (Helper.Checklogin(model.UserName, model.Password))
            {
                Session["username"] = model.UserName;
                if (model.RememberMe)
                {
                    HttpCookie ckUsername = new HttpCookie("username");
                    ckUsername.Expires = DateTime.Now.AddSeconds(3600);
                    ckUsername.Value = model.UserName;
                    Response.Cookies.Add(ckUsername);

                    HttpCookie ckPassword = new HttpCookie("password");
                    ckPassword.Expires = DateTime.Now.AddSeconds(3600);
                    ckPassword.Value = model.Password;
                    Response.Cookies.Add(ckPassword);
                }
                return RedirectToAction("Page", "User");
            }
            else
            {
                ViewBag.Error = "Account's Invalid";
                return View("Login");
            }
        }




        public ActionResult Logout()
        {
            //remove session
            Session.Remove("username");
            Session.Remove("UserId");
            //remove coockie
            if (Response.Cookies["username"] != null)
            {
                HttpCookie ckUsername = new HttpCookie("username");
                ckUsername.Expires = DateTime.Now.AddDays(-1d);
                Response.Cookies.Add(ckUsername);
            }
            if (Response.Cookies["password"] != null)
            {
                HttpCookie ckpassword = new HttpCookie("password");
                ckpassword.Expires = DateTime.Now.AddDays(-1d);
                Response.Cookies.Add(ckpassword);
            }
            return RedirectToAction("Login", "Account");
        }


        public LoginViewModel CheckCookie()
        {
            LoginViewModel user = null;
            string username = string.Empty, password = string.Empty;
            if (Request.Cookies["username"] != null)
                username = Request.Cookies["username"].Value;
            if (Request.Cookies["password"] != null)
                password = Request.Cookies["password"].Value;
            if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
                user = new LoginViewModel { UserName = username, Password = password };

            return user;
        }

        [HttpPost]
        public JsonResult CheckUserName(string UserName)
        {
            var user = context.Users.FirstOrDefault(x => x.UserName.ToUpper() == UserName.ToUpper());
            return Json(user == null);
        }

    }
}