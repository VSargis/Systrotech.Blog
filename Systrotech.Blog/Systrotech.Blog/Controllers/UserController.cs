﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Systrotech.Blog.Helpers;
using Systrotech.Blog.Mappers;
using Systrotech.Blog.View_Models;
using Systrotech_Repository.Entities;
using Systrotech_Repository.Services;

namespace Systrotech.Blog.Controllers
{
    public class UserController : Controller
    {
        private BlogEntities _context = new BlogEntities();
        // GET: User
        public ActionResult Question(int? id)
        {
            BlogRepository repContext = new BlogRepository();
            List<Category> allCategory = repContext.SelectAll<Category>();
            SelectListItem[] categories = Helper.GetCategories();

            Question question;
            if (id != null && id > 0)
            {
                // edit mode                
                question = repContext.GetById<Question>((int)id);
            }
            else
            {
                // add mode
                question = new Question();
            }

            return View(new AddQuestionViewModel { Categories = categories, Question = question });
        }

        [HttpPost]
        public ActionResult Question(AddQuestionViewModel model)
        {

            if (ModelState.IsValid)
            {
                model.Question.Datetime = DateTime.Now;
                if (model.Question.Id != 0)
                {
                    // edit mode->
                    // update db
                    Question question = model.Question;
                    try
                    {
                        question.UserId = (int)HttpContext.Session["UserId"];
                    }
                    catch (Exception)
                    {
                        return RedirectToAction("Login", "Account");
                    }
                    _context.Entry(question).State = System.Data.Entity.EntityState.Modified;
                    _context.SaveChanges();

                    return RedirectToAction("Login", "Account");
                }
                else
                {
                    // add mode
                    // insert db
                    Question question = model.Question;
                    try
                    {
                        question.UserId = (int)HttpContext.Session["UserId"];
                    }
                    catch (Exception)
                    {
                        return RedirectToAction("Login", "Account");
                    }
                    _context.Questions.Add(question);
                    _context.SaveChanges();

                    return RedirectToAction("Page", "User");
                }

            }
            return View();
        }


        public ActionResult Page()
        {
            return View();
        }

        public ActionResult List()
        {
            BlogRepository context = new BlogRepository();
            var lst = new List<Question>();
            lst.Add(context.GetById<Question>(1));
            return View(lst);
        }


    }
}