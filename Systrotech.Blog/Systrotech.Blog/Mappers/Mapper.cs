﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using Systrotech.Blog.Helpers;
using Systrotech.Blog.View_Models;
using Systrotech_Repository.Entities;
using Systrotech_Repository.Services;

namespace Systrotech.Blog.Mappers
{
    public static class Mapper
    {
        public static User ToRegisterViewModel(RegisterViewModel model)
        {
            SHA1 sha1hash = SHA1.Create();
            User registerUser = new User
            {
                Name = model.Name,
                Surname = model.Surname,
                Password = Helper.GetSha1Hash(sha1hash, model.Password), //hash
                Datetime = DateTime.Now,
                UserName = model.UserName,
                Email = model.Email
            };
            return registerUser;
        }
    }
}