﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Systrotech_Repository.Entities;
using Systrotech_Repository.Services;

namespace Systrotech.Blog.Helpers
{
    public static class Helper
    {

        public static string GetSha1Hash(SHA1 sha1Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash.
            byte[] data = sha1Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        // Verify a hash against a string.
        public static bool VerifySha1Hash(SHA1 sha1Hash, string input, string hash)
        {
            // Hash the input.
            string hashOfInput = GetSha1Hash(sha1Hash, input);

            // Create a StringComparer an compare the hashes.
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfInput, hash))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool Checklogin(string username, string password)// check login
        {
            BlogEntities context = new BlogEntities();
            var user = context.Users.FirstOrDefault(x => x.UserName.ToUpper() == username.ToUpper());// && x.Password == password);
            if (user == null)
            {
                return false;
            }
            else
            {
                SHA1 sha1Hash = SHA1.Create();
                bool isLogin = Helper.VerifySha1Hash(sha1Hash, password, user.Password);

                if (isLogin)
                {
                    HttpContext.Current.Session["UserId"] = user.Id;
                }

                return isLogin;
            }
        }

        public static SelectListItem[] GetCategories ()
        {
            BlogRepository context = new BlogRepository();
            List<Category> allCategory = context.SelectAll<Category>();
            SelectListItem[] categories = new SelectListItem[allCategory.Count];

            for (int i = 0; i < allCategory.Count; i++)
            {
                categories[i] = new SelectListItem { Text = $"{allCategory[i].Name}", Value = $"{allCategory[i].Id}" };
            }
            return categories;

        }

    }
}
