﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using Systrotech.Blog.Helpers;
using Systrotech_Repository.Entities;

namespace Systrotech.Blog.View_Models
{
    public class RegisterViewModel
    {
        
        [Display(Name = "Name")]
        [Required(ErrorMessage = "Name is required.")]
        [StringLength(50, ErrorMessage = "The Maximum length is 50")]
        public string Name { get; set; }

        [Display(Name = "Surname")]
        [StringLength(50, ErrorMessage = "The Maximum length is 50")]
        [Required(ErrorMessage = "Surname is requiredd.")]
        public string Surname { get; set; }

        [Display(Name = "UserName")]
        [StringLength(50, MinimumLength = 4, ErrorMessage = "UserName must be between 4 and 50 characters")]
        [Required(ErrorMessage = "UserName is required")]
        [System.Web.Mvc.Remote("CheckUserName", "Account", HttpMethod = "POST", ErrorMessage = "User name already exists. Please enter a different user name.")]
        public string UserName { get; set; }

        [Display(Name = "Email")]
        [Required(ErrorMessage = "Email is requiredd.")]
        [RegularExpression(@"^[a-z][a-z|0-9|]*([_][a-z|0-9]+)*([.][a-z|0-9]+([_][a-z|0-9]+)*)?@[a-z][a-z|0-9|]*\.([a-z][a-z|0-9]*(\.[a-z][a-z|0-9]*)?)$", ErrorMessage = "Please enter valid email")]
        public string Email { get; set; }


        [Display(Name = "Password")]
        [MinLength(6, ErrorMessage = "Password must be more then 6  characters")]
        [MaxLength(128, ErrorMessage = "Password must be less then 64 characters")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Password is required.")]
        public string Password { get; set; }


        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password")]
        [Compare("Password", ErrorMessage = "Confirm password doesn't match, Type again !")]
        public string ConfirmPassword { get; set; }
    }
}