﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using Systrotech.Blog.Helpers;
using Systrotech_Repository.Entities;

namespace Systrotech.Blog.View_Models
{
    public class LoginViewModel
    {

        [Display(Name = "UserName")]
        [Required(ErrorMessage = "UserName is required")]
        //[System.Web.Mvc.Remote("Register", "Home", HttpMethod = "POST", ErrorMessage = "User name already exists. Please enter a different user name.")]
        public string UserName { get; set; }

        [Display(Name = "Password")]
        [MinLength(6, ErrorMessage = "Password must be more then 6  characters")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Password is required.")]
        public string Password { get; set; }

        [Display(Name = "Remember Me")]
        public bool RememberMe { get; set; }


        
    }
}